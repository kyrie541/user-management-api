Install dependency<br />
$ npm install

<br>
////////////////////////////////////////////////////////////////////////
<br>

How to start the server?<br />
$ node server.js<br />

<br>
////////////////////////////////////////////////////////////////////////
<br>
Seed user<br />
<br>
1) install seeding cli<br />
$ npm install -g mongo-seeding-cli<br />

2) seed data command <br />
$ seed -u mongodb://127.0.0.1:27017/user-management-api ./seeders <br />

(if u attempt to login, here is the credential) <br />
fake_username <br />
Secret123# <br />

<br>
////////////////////////////////////////////////////////////////////////
<br>
Install Mongodb in your local (im using mongodb)<br />
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/<br />
