const express = require("express");
const router = express.Router();
const UserController = require('../controllers/UserController');

router.get('/', UserController.userList);
router.post('/', UserController.userCreate);
router.get('/:userId', UserController.userShow);
router.put('/:userId/update', UserController.userUpdate);
router.delete('/:userId/delete', UserController.userDelete);

module.exports = router;