const express = require('express');
const cors = require('cors');

const authMiddleware = require('./middlewares/AuthMiddleware');

const authRoute = require("./routes/AuthRoute");
const userRoute = require("./routes/UserRoute");

const app = express();
const port = 3001;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.use('/api/auth', authRoute);
app.use('/api/user', authMiddleware.loginRequired, userRoute);

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})