const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: 'Name cannot be blank!',
        unique: true
    },
    password: {
        type: String,
        required: 'Password cannot be blank!'
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;