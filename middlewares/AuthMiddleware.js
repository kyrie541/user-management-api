const { secret_key } = require('../env');
const jwt = require("jsonwebtoken");

module.exports = {
    loginRequired: (req, res, next) => {
        try {
            const token = req.headers.authorization.split(" ")[1];
            jwt.verify(token, secret_key, function (err, decoded) {
                if (decoded) {
                    next();
                } else {
                    res.send('Please Login');
                }
            });
        } catch (e) {
            res.status(400).send('Bad Credential')
        }
    },
}