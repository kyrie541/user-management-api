const { secret_key } = require('../env');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require('../models');

exports.loginUser = (req, res) => {
    db.User.findOne({
        username: req.body.username
    })
        .then(user => {
            bcrypt.compare(req.body.password, user.password).then(response => {
                if (response === true) {
                    const token = jwt.sign({ userId: user.id, username: user.username }, secret_key, {
                        expiresIn: 86400,
                    });
                    res.json({ token })
                } else {
                    res.status(400).send('Wrong Credential')
                }
            });
        })
        .catch(err => {
            console.log('err', err);
            res.status(400).send('Bad Request')
        });
};

module.exports = exports;