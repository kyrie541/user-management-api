const db = require('../models');
const bcrypt = require("bcryptjs");
const { pickBy } = require('lodash');

exports.userList = (req, res) => {
    db.User.find()
        .then((users) => {
            res.json({ users });
        })
        .catch((err) => {
            res.send('fail to find users');
        });
}

exports.userShow = (req, res) => {
    db.User.findById(req.params.userId)
        .then(function (foundUser) {
            res.json(foundUser);
        })
        .catch(function (err) {
            res.send(err);
        })
}


exports.userCreate = (req, res) => {
    bcrypt
        .hash(req.body.password, parseInt(12))
        .then(function (hashedPassword) {
            db.User.create({
                ...req.body,
                password: hashedPassword,
            }).then((user) => {
                res.send({ message: 'user create sucess', userId: user.id })
            }).catch((err) => {
                res.send('fail to create user')
            });;
        })
        .catch(() => { console.error('fail to hash password') });

}

exports.userUpdate = (req, res) => {
    const updateUser = (data) => {
        db.User.findOneAndUpdate({ _id: req.params.userId }, data, { new: true })
            .then(function (user) {
                res.json(user);
            })
            .catch(function (err) {
                res.send(err);
            })
    }

    if (req.body.password) {
        bcrypt
            .hash(req.body.password, parseInt(12))
            .then(function (hashedPassword) {
                updateUser({ ...req.body, password: hashedPassword })
            })
            .catch(() => { res.send('fail to hash password'); });
    } else {
        updateUser(pickBy(req.body));
    }
}

exports.userDelete = (req, res) => {
    db.User.deleteOne({ _id: req.params.userId })
        .then(() => {
            res.send({ message: 'user delete sucess' })
        })
        .catch((err) => {
            res.send('fail to delete user')
        });
}

module.exports = exports;